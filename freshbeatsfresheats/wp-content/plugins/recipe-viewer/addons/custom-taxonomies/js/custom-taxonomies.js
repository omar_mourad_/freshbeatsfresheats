jQuery(document).ready(function() {
    jQuery('.wprv-edit-tag').on('click', function() {
        var tag = jQuery(this).data('tag');

        var singular = jQuery(this).parents('tr').find('.singular-name').text();
        var name = jQuery(this).parents('tr').find('.name').text();
        var slug = jQuery(this).parents('tr').find('.slug').text();

        jQuery('input#wprv_edit_tag_name').val(tag);
        jQuery('input#wprv_custom_taxonomy_singular_name').val(singular);
        jQuery('input#wprv_custom_taxonomy_name').val(name);
        jQuery('input#wprv_custom_taxonomy_slug').val(slug);

        jQuery('#wprv_editing_tag').text(tag);

        jQuery('.wprv_adding').hide();
        jQuery('.wprv_editing').show();
    });

    jQuery('#wprv_cancel_editing').on('click', function() {
        jQuery('input#wprv_edit_tag_name').val('');
        jQuery('input#wprv_custom_taxonomy_singular_name').val('');
        jQuery('input#wprv_custom_taxonomy_name').val('');
        jQuery('input#wprv_custom_taxonomy_slug').val('');

        jQuery('.wprv_adding').show();
        jQuery('.wprv_editing').hide();
    });
});