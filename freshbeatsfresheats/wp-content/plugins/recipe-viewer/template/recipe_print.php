<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title></title>
    <script>
        var wprv = window.opener.wprv;

        document.title = wprv.print_template.title;

        for(var font in wprv.print_template.fonts) {
            var link = wprv.print_template.fonts[font];
            document.write('<link rel="stylesheet" type="text/css" href="'+link+'">');
        }

        document.write('<link rel="stylesheet" type="text/css" href="'+wprv.pluginUrl+'/css/print.css">');
        document.write('' +
            '<style>' +
            '.print-header { font-family: '+ wprv.print_template.header.font +'; font-style: '+ wprv.print_template.header.style +'; font-weight: '+ wprv.print_template.header.weight +'; font-size: '+ wprv.print_template.header.size +'px; } ' +
            '.wprv-container { font-family: '+ wprv.print_template.recipe.font +'; font-style: '+ wprv.print_template.recipe.style +'; font-weight: '+ wprv.print_template.recipe.weight +'; font-size: '+ wprv.print_template.recipe.size +'px; } ' +
            '.print-footer { font-family: '+ wprv.print_template.recipe.font +'; font-size: '+ wprv.print_template.recipe.size +'px; } ' +
            wprv.custom_print_css +
            '</style>');
    </script>
</head>
<body onload="setTimeout(function(){window.print()}, 500);">
<script>
    if(wprv.print_template.logo !== '') {
        document.write('<img class="print-logo" src="'+wprv.print_template.logo+'" />');
    }
</script>
<script>
    if(wprv.print_template.header.text !== '') {
        document.write('<div class="print-header">');
        document.write(wprv.print_template.header.text);
        document.write('</div>');
    }
</script>

<div class="wprv-container">
    <script>
        document.write(window.opener.parentRecipe);
    </script>
</div>

<script>
    if(wprv.print_template.footer !== '') {
        document.write('<div class="print-footer">');
        document.write(wprv.print_template.footer);
        document.write('</div>');
    }
</script>
</body>
</html>