<?php
/**
 * Plugin Name: Sql Dump App
 * Description: Dump the database to a sql folder under wordpress.
 * Version: 1.0
 * Author: Code Gears Pro
 */
define( "SDA_NAME", 'Sql Dump App');
 
/*Activation*/
function sda_activation() {
	/*
	if (get_option('upload_path') == '' || get_option('upload_url_path') == '') {
		update_option('upload_path', 'wp-content/uploads', true);
		update_option('upload_url_path', home_url().'/wp-content/uploads', true);
	}*/
	update_option('sda_dump_path', get_home_path() . 'sql/wordpress.sql', true);
	update_option('sda_dump_path_default', get_home_path() . 'sql/wordpress.sql', true);
}
register_activation_hook( __FILE__, 'sda_activation');


/*Translate*/
//load_plugin_textdomain( 'sda', false, dirname(plugin_basename(__FILE__)).'/lang/'); 

/*Link Settings in plugins lists*/
function sda_settings_link($links, $file) { 
	if ( $file == plugin_basename(dirname(__FILE__)).'/sql-dump-app.php' ) {
	   $settings_link = '<a href="'.admin_url('options-general.php?page=sda-settings').'">'.__('Settings').'</a>'; 
	   array_unshift($links, $settings_link); 
	}
	return $links; 
}
add_filter("plugin_action_links", 'sda_settings_link', 10, 2 );

function sda_settings_subpage() {
	add_submenu_page( 'options-general.php', SDA_NAME, SDA_NAME, 'manage_options', 'sda-settings', 'sda_settings_options' );
}
add_action('admin_menu', 'sda_settings_subpage');

/*Options Pages*/
function sda_settings_options() {

	/*Variables*/
	$notice_update = $notice_uninstall = false;

	/*Check form*/
	/*New settings*/
	if (isset($_POST['sda-settings']) && check_admin_referer('sda-settings')) {
		if (empty($_POST['sda_dump_path'])) {
			update_option('sda_dump_path', get_option('sda_dump_path_default'), true);
		} else {
			update_option('sda_dump_path', $_POST['sda_dump_path'], true);
		}
		
		$DATABASE=DB_NAME;
		
		$filename = "backup-" . date("d-m-Y") . ".sql.gz";
		$mime = "application/x-gzip";
		
		//$cmd = "mysqldump -u ".DB_USER." --password=".DB_PASSWORD." $DATABASE | gzip --best";
		$dumpPath = get_option('sda_dump_path');
		$cmd = "mysqldump -u ".DB_USER." --password=".DB_PASSWORD." $DATABASE > $dumpPath";
		
		passthru( $cmd );
		
		$notice_update = true;
	}
	/*Uninstall*/
	if (isset($_POST['sda-uninstall']) && check_admin_referer('sda-uninstall')) {
		update_option('sda_dump_path', '', true);
		update_option('sda_dump_path_default', '', true);
		update_option('sda_database_name', '', true);
			
		$notice_uninstall = true;
		$url_uninstall = wp_nonce_url(admin_url('plugins.php?action=deactivate&plugin=wp-original-media-path/wp_original_media_path.php&plugin_status=all&paged=1'),'deactivate-plugin_wp-original-media-path/wp_original_media_path.php');
			
	}

	/*Notice*/
	if ($notice_update == true) {
		echo '<div id="message" class="updated fade"><p><strong>'.__('Database Dumped.','sda').'</strong></p></div>';
	}
	else if ($notice_uninstall == true) {
		echo '<div id="message" class="updated fade"><p><strong>'.__('Uninstall completed.','sda').'</strong><br/><a href="'.$url_uninstall.'">'.__('Click here now to disable the plugin','sda').'</a></p></div>';
	}
	?>
	<style type="text/css">
		.button {width: auto!important;}
		#validation {border-top: 1px solid #dfdfdf;padding: 10px 0}
	</style>
	<div class="wrap" id="sda-settings">
		<div id="icon-options-general" class="icon32"><br></div>
		<h2><?php echo SDA_NAME; ?></h2>
		<div id="poststuff" class="metabox-holder has-right-sidebar">
			<div id="side-info-column" class="inner-sidebar">
			    <div id="linksubmitdiv" class="postbox " style="">
			        <h3 class="hndle"><span><?php _e('Uninstall','sda'); ?></span></h3>
			        <div class="inside">
			            <p><?php _e('Click to delete the settings and disable the plugin','sda');?>.</p>
			            <form method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>">
			            	<?php wp_nonce_field('sda-uninstall');?>
			            	<input type="submit" value="<?php _e('Uninstall now','sda');?>" class="button" name="sda-uninstall"/>
			            </form>
			        </div>
			    </div>
			</div>
			<div id="post-body">
			    <div id="post-body-content">
			        <div id="namediv" class="stuffbox">
			            <h3><label for="link_name"><?php _e('Settings','sda'); ?></label></h3>
			            <div class="inside">
							<form method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>" >
								<table class="form-table">
									<tr valign="top">
										<th scope="row">
											<label for="upload_path"><?php _e('Folder where the Database will be dumped','sda'); ?></label>
										</th>
										<td>
											<input name="sda_dump_path" type="text" id="sda_dump_path" value="<?php echo esc_attr(get_option('sda_dump_path')); ?>" class="regular-text code" />
											<p class="description"><?php echo esc_attr(get_option('sda_dump_path_default')); ?></p>
										</td>
									</tr>
								</table>
								<div id="validation">
									<?php wp_nonce_field('sda-settings');?>
									<input type="submit" value="<?php _e('Dump Database','sda');?>" class="button-primary button" name="sda-settings"/>
								</div>
							</form>
			            </div>
			        </div>
			    </div>
			</div>
		</div>

	</div>
<?php } 












