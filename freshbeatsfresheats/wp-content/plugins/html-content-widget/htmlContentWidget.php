<?php
/*
 Plugin Name: Html Content Widget
Plugin URI:
Description: HTML content widget.  This widget will show any HTML content.
Version: 1.1.1
Author: Code Gears Pro
Author URI:
*/
if ( ! class_exists( 'HtmlContentWidget' ) ) {

	class HtmlContentWidget extends WP_Widget {
		private $parameters;
		
		/**
		 * Sets up the widgets name etc
		 */
		public function __construct() {
			$widget_ops = array('classname' => 'HtmlContentWidget', 'description' => 'This widget will show any HTML content.' );
			$control_ops = array('width' => 400, 'height' => 350);
			parent::__construct('HtmlContentWidget', __('Html Content'), $widget_ops, $control_ops);
		}
		
		/**
		 * Outputs the content of the widget
		 *
		 * @param array $args
		 * @param array $instance
		 */
		public function widget( $args, $instance ) {
			extract($args);
			//$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );
			$text = apply_filters( 'widget_text', empty( $instance['text'] ) ? '' : $instance['text'], $instance );
			echo $before_widget;
			//if ( !empty( $title ) ) { echo $before_title . $title . $after_title; } ?>
				<div class="textwidget"><?php echo !empty( $instance['filter'] ) ? wpautop( $text ) : $text; ?></div>
			<?php
			echo $after_widget;
		}
		
		/**
		 * Ouputs the options form on admin
		 *
		 * @param array $instance The widget options
		 */
		public function form( $instance ) {
			$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'text' => '' ) );
			//$title = strip_tags($instance['title']);
			$text = esc_textarea($instance['text']);
			
			?>
		
		<textarea class="widefat" rows="16" cols="20" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>"><?php echo $text; ?></textarea>

<?php
		}
		
		/**
		 * Processing widget options on save
		 *
		 * @param array $new_instance The new options
		 * @param array $old_instance The previous options
		 */
		public function update( $new_instance, $old_instance ) {
			$instance = $old_instance;
			//$instance['title'] = strip_tags($new_instance['title']);
			if ( current_user_can('unfiltered_html') )
				$instance['text'] =  $new_instance['text'];
			else
				$instance['text'] = stripslashes( wp_filter_post_kses( addslashes($new_instance['text']) ) ); // wp_filter_post_kses() expects slashed
			//$instance['filter'] = isset($new_instance['filter']);
			return $instance;
		}
	}
}

add_filter( 'widget_text', 'do_shortcode' );
add_action( 'widgets_init', create_function('', 'return register_widget("HtmlContentWidget");') );
