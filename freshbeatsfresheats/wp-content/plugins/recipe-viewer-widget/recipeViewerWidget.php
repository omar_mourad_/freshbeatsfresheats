<?php
/*
Plugin Name: Recipe Viewer Widget
Plugin URI:
Description: Recipe Viewer widget.  This widget will return the last recipes.
Version: 1.1.1
Author: Code Gears Pro
Author URI:
*/
if ( ! class_exists( 'RecipeViewerWidget' ) ) {
	define(MAX_TITLE_LENGTH, 20);
	
	function rvw_get_limited_excerpt($excerpt, $maxLenght) {
		$excerpt = strip_shortcodes($excerpt);
		$excerpt = strip_tags($excerpt);
	
		if (strlen($excerpt) > $maxLenght) {
			$excerpt = substr($excerpt, 0, $maxLenght) . '...';
		}
		if (empty($excerpt)) {
			$excerpt = '&nbsp;';
		}
	
		return $excerpt;
	}

	class RecipeViewerRecentWidget extends WP_Widget {
		
		public function __construct() {
			$widget_ops = array('classname' => 'widget_recipe_viewer_recent', 'description' => __( "Your site&#8217;s most recent recipes.") );
			parent::__construct('recipe_viewer_recent', __('Recent Recipes'), $widget_ops);
			$this->alt_option_name = 'widget_recipe_viewer_recent_entries';
		
			add_action( 'save_post', array($this, 'flush_widget_cache') );
			add_action( 'deleted_post', array($this, 'flush_widget_cache') );
			add_action( 'switch_theme', array($this, 'flush_widget_cache') );
		}
		
		public function widget($args, $instance) {
			$cache = array();
			if ( ! $this->is_preview() ) {
				$cache = wp_cache_get( 'widget_recipe_viewer_recent_posts', 'widget' );
			}
		
			if ( ! is_array( $cache ) ) {
				$cache = array();
			}
		
			if ( ! isset( $args['widget_id'] ) ) {
				$args['widget_id'] = $this->id;
			}
		
			if ( isset( $cache[ $args['widget_id'] ] ) ) {
				echo $cache[ $args['widget_id'] ];
				return;
			}
		
			ob_start();
		
			$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Recent Recipes' );
		
			/** This filter is documented in wp-includes/default-widgets.php */
			$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
		
			$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
			if ( ! $number )
				$number = 5;
			$show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : false;
			$show_comments = isset( $instance['show_comments'] ) ? $instance['show_comments'] : false;
		
			/**
			 * Filter the arguments for the Recent Posts widget.
			 *
			 * @since 3.4.0
			 *
			 * @see WP_Query::get_posts()
			 *
			 * @param array $args An array of arguments used to retrieve the recent posts.
			 */
			//post_type="recipe"
			$r = new WP_Query( apply_filters( 'widget_posts_args', array(
					'posts_per_page'      => $number,
					'no_found_rows'       => true,
					'post_status'         => 'publish',
					'ignore_sticky_posts' => true,
					'post_type' 		  => 'recipe'
			) ) );
			
			$image_size = array(90,90);
			
			if ($r->have_posts()) :
			?>
				<?php echo $args['before_widget']; ?>
				<?php if ( $title ) {
					echo $args['before_title'] . $title . $args['after_title'];
				} ?>
				
				<ul>
				<?php 
				while ( $r->have_posts() ) : $r->the_post(); 
					$_post = get_post(get_the_ID());
					if ($_post->comment_status !== 'open') {
						$commentsText = 'Comments Off';
					} else {
						if ($_post->comment_count > 0) {
							$commentsText = $_post->comment_count . ' comments';
						} else {
							$commentsText = 'No comments';
						}
					}
					
					$title = rvw_get_limited_excerpt(get_the_title() ? the_title('', '', false) : get_the_ID(), MAX_TITLE_LENGTH);
					$image = get_the_post_thumbnail(get_the_ID(), $image_size);
				?>
					
					<li>
						<a href="<?php the_permalink(); ?>">
						<div class="image">
							<?php echo $image ?>
						</div>
						<div class="content">
							<span class="recipe-title"><h3><?php echo $title ?></h3></span>
							<?php if ( $show_date ) : ?>
								<span class="recipe-date">Posted on <?php echo get_the_date('n/j/Y') ?></span>
							<?php endif; ?>
							<?php if ( $show_comments ) : ?>
								<span class="recipe-comments"><?php echo $commentsText?></span>
							<?php endif; ?>
						</div>
						</a>
					</li>
				<?php endwhile; ?>
				</ul>
				
				<?php echo $args['after_widget']; ?>
		<?php
				// Reset the global $the_post as this query will have stomped on it
				wp_reset_postdata();
		
				endif;
		
				if ( ! $this->is_preview() ) {
					$cache[ $args['widget_id'] ] = ob_get_flush();
					wp_cache_set( 'widget_recipe_viewer_recent_posts', $cache, 'widget' );
				} else {
					ob_end_flush();
				}
			}
		
			public function update( $new_instance, $old_instance ) {
				$instance = $old_instance;
				$instance['title'] = strip_tags($new_instance['title']);
				$instance['number'] = (int) $new_instance['number'];
				$instance['show_date'] = isset( $new_instance['show_date'] ) ? (bool) $new_instance['show_date'] : false;
				$instance['show_comments'] = isset( $new_instance['show_comments'] ) ? (bool) $new_instance['show_comments'] : false;
				$this->flush_widget_cache();
		
				$alloptions = wp_cache_get( 'alloptions', 'options' );
				if ( isset($alloptions['widget_recipe_viewer_recent_entries']) )
					delete_option('widget_recipe_viewer_recent_entries');
		
				return $instance;
			}
		
			public function flush_widget_cache() {
				wp_cache_delete('widget_recipe_viewer_recent_posts', 'widget');
			}
		
			public function form( $instance ) {
				$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
				$number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
				$show_date = isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : false;
				$show_comments = isset( $instance['show_comments'] ) ? (bool) $instance['show_comments'] : false;
		?>
				<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>
		
				<p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:' ); ?></label>
				<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>
		
				<p><input class="checkbox" type="checkbox" <?php checked( $show_date ); ?> id="<?php echo $this->get_field_id( 'show_date' ); ?>" name="<?php echo $this->get_field_name( 'show_date' ); ?>" />
				<label for="<?php echo $this->get_field_id( 'show_date' ); ?>"><?php _e( 'Display post date?' ); ?></label></p>
				
				<p><input class="checkbox" type="checkbox" <?php checked( $show_comments ); ?> id="<?php echo $this->get_field_id( 'show_comments' ); ?>" name="<?php echo $this->get_field_name( 'show_comments' ); ?>" />
				<label for="<?php echo $this->get_field_id( 'show_comments' ); ?>"><?php _e( 'Display comments count?' ); ?></label></p>
		<?php
			}
	}
	
	class RecipeViewerArchiveWidget extends WP_Widget {
	
		public function __construct() {
			$widget_ops = array('classname' => 'widget_recipe_viewer_archive', 'description' => __( "Your site&#8217;s monthly archive of recipes.") );
			parent::__construct('recipe_viewer_archive', __('Monthly Archive Recipes'), $widget_ops);
			$this->alt_option_name = 'widget_recipe_viewer_archive_entries';
	
			add_action( 'save_post', array($this, 'flush_widget_cache') );
			add_action( 'deleted_post', array($this, 'flush_widget_cache') );
			add_action( 'switch_theme', array($this, 'flush_widget_cache') );
		}
	
		public function widget($args, $instance) {
			global $wpdb, $wp_locale;
			$cache = array();
			if ( ! $this->is_preview() ) {
				$cache = wp_cache_get( 'widget_recipe_viewer_archive_posts', 'widget' );
			}
	
			if ( ! is_array( $cache ) ) {
				$cache = array();
			}
	
			if ( ! isset( $args['widget_id'] ) ) {
				$args['widget_id'] = $this->id;
			}
	
			if ( isset( $cache[ $args['widget_id'] ] ) ) {
				echo $cache[ $args['widget_id'] ];
				return;
			}
	
			ob_start();
	
			$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Monthly Archive Recipes' );
	
			/** This filter is documented in wp-includes/default-widgets.php */
			$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
			
			
			
			
			$defaults = array(
					'type' => 'monthly', 'limit' => '',
					'format' => 'html', 'before' => '',
					'after' => '', 'show_post_count' => false,
					'echo' => 1, 'order' => 'DESC',
			);

			$r = wp_parse_args( '', $defaults );
			
			if ( '' == $r['type'] ) {
				$r['type'] = 'monthly';
			}
			
			if ( ! empty( $r['limit'] ) ) {
				$r['limit'] = absint( $r['limit'] );
				$r['limit'] = ' LIMIT ' . $r['limit'];
			}
			
			$order = strtoupper( $r['order'] );
			if ( $order !== 'ASC' ) {
				$order = 'DESC';
			}
			
			/**
			 * Filter the SQL WHERE clause for retrieving archives.
			 *
			 * @since 2.2.0
			 *
			 * @param string $sql_where Portion of SQL query containing the WHERE clause.
			 * @param array  $r         An array of default arguments.
			 */
			$where = apply_filters( 'getarchives_where', "WHERE post_type = 'recipe' AND post_status = 'publish'", $r );
			
			/**
			 * Filter the SQL JOIN clause for retrieving archives.
			 *
			 * @since 2.2.0
			 *
			 * @param string $sql_join Portion of SQL query containing JOIN clause.
			 * @param array  $r        An array of default arguments.
			*/
			$join = apply_filters( 'getarchives_join', '', $r );
			
			
			
			$query = "SELECT YEAR(post_date) AS `year`, MONTH(post_date) AS `month`, count(ID) as posts FROM $wpdb->posts $join $where GROUP BY YEAR(post_date), MONTH(post_date) ORDER BY post_date $order $limit";
			$key = md5( $query );
			$key = "widget_recipe_viewer_archive_get_archives:$key:$last_changed";
			if ( ! $results = wp_cache_get( $key, 'posts' ) ) {
				$results = $wpdb->get_results( $query );
				wp_cache_set( $key, $results, 'posts' );
			}
			?>
				<?php echo $args['before_widget']; ?>
				<?php if ( $title ) {
					echo $args['before_title'] . $title . $args['after_title'];
				} ?>
				
				<ul>
				<?php 
			if ( $results ) {
					foreach ( (array) $results as $result ) {
						$url = get_month_link( $result->year, $result->month ) . '?post_type=recipe';
						$text = sprintf( __( '%1$s %2$d' ), $wp_locale->get_month( $result->month ), $result->year );
					?>
						
						<li>
							<a href="<?php echo $url ?>">
							<div class="content">
								<span class="recipe-title"><?php echo $text ?></span>
							</div>
							</a>
						</li>
					<?php 
					} ?>
					
			<?php
			}?>
			</ul>
				
			<?php echo $args['after_widget']; 
			
				}
			
				public function update( $new_instance, $old_instance ) {
					$instance = $old_instance;
					$instance['title'] = strip_tags($new_instance['title']);
					$this->flush_widget_cache();
			
					$alloptions = wp_cache_get( 'alloptions', 'options' );
					if ( isset($alloptions['widget_recipe_viewer_archive_entries']) )
						delete_option('widget_recipe_viewer_archive_entries');
			
					return $instance;
				}
			
				public function flush_widget_cache() {
					wp_cache_delete('widget_recipe_viewer_archive_posts', 'widget');
				}
			
				public function form( $instance ) {
					$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
			?>
					<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
					<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>
			
			<?php
				}
		}
		
	class RecipeViewerCoursesWidget extends WP_Widget {
	
		public function __construct() {
			$widget_ops = array('classname' => 'widget_recipe_viewer_courses', 'description' => __( "Your site&#8217;s recipes per courses.") );
			parent::__construct('recipe_viewer_courses', __('Courses Recipes'), $widget_ops);
			$this->alt_option_name = 'widget_recipe_viewer_courses_entries';
	
			add_action( 'save_post', array($this, 'flush_widget_cache') );
			add_action( 'deleted_post', array($this, 'flush_widget_cache') );
			add_action( 'switch_theme', array($this, 'flush_widget_cache') );
		}
	
		public function widget($args, $instance) {
			global $wpdb, $wp_locale;
			$cache = array();
			if ( ! $this->is_preview() ) {
				$cache = wp_cache_get( 'widget_recipe_viewer_courses_posts', 'widget' );
			}
	
			if ( ! is_array( $cache ) ) {
				$cache = array();
			}
	
			if ( ! isset( $args['widget_id'] ) ) {
				$args['widget_id'] = $this->id;
			}
	
			if ( isset( $cache[ $args['widget_id'] ] ) ) {
				echo $cache[ $args['widget_id'] ];
				return;
			}
	
			ob_start();
	
			$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Courses Recipes' );
	
			/** This filter is documented in wp-includes/default-widgets.php */
			$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
			
			//taxonomy=course&post_type=recipe
			$results = get_categories( array('taxonomy' => 'course', 'post_type' => 'recipe') );
			
			?>
							<?php echo $args['before_widget']; ?>
							<?php if ( $title ) {
								echo $args['before_title'] . $title . $args['after_title'];
							} ?>
							
							<ul>
							<?php 
						if ( $results ) {
								foreach ( (array) $results as $result ) {
									$url = '/course/'.$result->slug.'/';
									$text = $result->name;
									//$url = get_month_link( $result->year, $result->month ) . '?post_type=recipe';
									//$text = sprintf( __( '%1$s %2$d' ), $wp_locale->get_month( $result->month ), $result->year );
								?>
									
									<li>
										<a href="<?php echo $url ?>">
										<div class="content">
											<span class="recipe-title"><?php echo $text ?></span>
										</div>
										</a>
									</li>
								<?php 
								} ?>
								
						<?php
						}?>
						</ul>
							
						<?php echo $args['after_widget']; 
		}
	
		public function update( $new_instance, $old_instance ) {
			$instance = $old_instance;
			$instance['title'] = strip_tags($new_instance['title']);
			$this->flush_widget_cache();
	
			$alloptions = wp_cache_get( 'alloptions', 'options' );
			if ( isset($alloptions['widget_recipe_viewer_courses_entries']) )
				delete_option('widget_recipe_viewer_courses_entries');
	
			return $instance;
		}
	
		public function flush_widget_cache() {
			wp_cache_delete('widget_recipe_viewer_courses_posts', 'widget');
		}
	
		public function form( $instance ) {
			$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
	?>
			<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>
	
	<?php
		}
	}
		
	add_filter( 'widget_text', 'do_shortcode' );
	add_action( 'widgets_init', create_function('', 'return register_widget("RecipeViewerRecentWidget");') );
	add_action( 'widgets_init', create_function('', 'return register_widget("RecipeViewerArchiveWidget");') );
	add_action( 'widgets_init', create_function('', 'return register_widget("RecipeViewerCoursesWidget");') );
}


