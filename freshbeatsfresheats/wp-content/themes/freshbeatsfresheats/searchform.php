

<?php
$url = esc_url( home_url( '/' ) );
$placeholder = '';

if (empty($class)) {
	$class = 'search-form';
}

if ($addRescipePostType) {
	$extraInputs = '<input type="hidden" value="recipe" name="post_type"  />';
	$addRescipePostType = false;
}



$searchQuery = get_search_query();

$form = <<<HTML
<form role="search" method="get" class="$class" action="$url">
	<input type="search" class="search-field" placeholder="$placeholder" value="$searchQuery" name="s" title="$placeholder" />
	$extraInputs
	<input type="submit" class="search-submit" value="" />
</form>
HTML;

echo $form;