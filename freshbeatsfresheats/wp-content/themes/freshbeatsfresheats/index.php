<?php get_header(); debug(__FILE__);?>
<div class="subcontentindex">
	<div class="maincontent">
		<div class="center page" id="post-<?php the_ID(); ?>">
		<h1><?php the_title(); ?></h1>
		
		<?php ?>
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
		<span>Posted by <?php echo get_the_author()?></span>
		<span>on <?php echo get_the_date('(n/j/Y)')?></span>
		
		<?php the_post_thumbnail(); ?>
		<div class="recipe_content">
			<?php the_content('<p>Read the rest of this entry &raquo;</p>');?>
			
			<span class="comments-link">Tags: <?php echo cgp_get_the_term_list(get_the_ID(), 'post_tag', '<span class="tag-links">', ', ', '</span>', '?post_type=recipe'); ?></span>
			<span class="comments-link"><?php //comments_popup_link( __( 'Leave a comment', 'twentyfourteen' ), __( '1 Comment', 'twentyfourteen' ), __( '% Comments', 'twentyfourteen' ) ); ?></span>
			<div class="comment-section">
				<?php comments_template(); ?>
			</div>
		
		</div>
		
		<?php endwhile; else: ?>

			<p>Sorry, no posts.</p>
		
		<?php endif; ?>
		</div>
		
		<?php get_template_part( 'sidebar', 'content' ); ?>
		
	</div>
	
	

<?php get_footer(); ?>	
