<?php
/*
Template Name: Recipes Home Template
*/
?>
<?php get_header(); debug(__FILE__);?>

<div class="subcontentindex">
	<div id="content" class="content-results">
			<div id="content" class="contentPage recipes home">
				
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
					<div id="topnav">
						<h1><?php the_title(); ?></h1>
					</div>
					<div class="featured-divider"></div>
					<div class="recipe cards" >
					<?php the_content('<p>Read the rest of this entry &raquo;</p>'); ?>
					</div>
				</div>

<?php endwhile; else: ?>

		<p>Sorry, no posts.</p>

<?php endif; ?>
			</div>
	</div>
</div>

<?php get_footer(); ?>	
