<?php
/*
Template Name: HomePage
*/
?>
<?php get_header(); debug(__FILE__);?>
		<div class="slideshow">
			<div>
				<?php 
				if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('home-page-slider-text') ) :
				endif;
				?>
			</div>
		</div>
		<div class="subcontentindex">
			<div class="maincontent">
				<div class="center">
				<?php 
if (have_posts()) {
	while (have_posts()) {
		the_post();
		the_content();
	}
}
				?>
				</div>
				
				<?php get_template_part( 'sidebar', 'content' ); ?>
				
			</div>
		
<style>
.flex-control-nav {
    bottom: 10px;
}
</style>
<?php get_footer(); ?>	
