<?php get_header(); debug(__FILE__);

$optionalMessageHtml = <<<HTML
<div class='recipe_item' >
	<div class='thumbnail'>%thumbnail%</div>
	<div class='content'>
		<span class='title' >
			<a href='%url%'>%title%</a>
		</span>
		<span class='info-line' >&nbsp;</span>
		<span class='excerpt' >%excerpt%<a class='read_more' href='%url%'>Read more ...</a>
		</span>
	</div>
</div>
HTML;

$atts = array(
		'post_type'			=> 'recipe',
		'date_format'         => 'F j Y g:ia',
		'special_date_format' => '\<\s\p\a\n\>D\<\/\s\p\a\n\>\<\s\p\a\n\>d\<\/\s\p\a\n\>',
		'image_size'          => 'custom_250_250',
		'include_title'       => true,
		'include_content'     => false,
		'include_date'        => true,
		'include_excerpt'     => true,
		'include_wrapper'     => false,
		'optional_message'    => $optionalMessageHtml,
		'no_posts_message'    => 'Currently we do not have any Recipes.',
		'wrapper'             => 'ul',
		'excerpt_size'        => 250,
		'title_size'          => 0,
		'include_wrapper'     => 0,
);
global $wp_query;

$wp_query->rewind_posts();
$html = cgp_display_posts_shortcode_display($wp_query, $atts);
?>
<div class="subcontentindex">
	<div class="maincontent">
		<div class="center">
		<h1><?php 
		$category = single_cat_title('', false);
		if ( is_day() ) :
			printf('Daily Archives: %s', get_the_date() );
			
		elseif ( is_month() ) :
			printf('Monthly Archives: %s', get_the_date( _x( 'F Y', 'monthly archives date format', 'twentyfourteen' ) ) );
			
		elseif ( is_year() ) :
			printf('Yearly Archives: %s', get_the_date( _x( 'Y', 'yearly archives date format', 'twentyfourteen' ) ) );
			
		elseif (!empty($category)):
			printf( $category . ' tag archives');
				
		else :
			printf( ' Archives');
			
		endif; ?></h1>
		<?php echo $html; ?>
		</div>
		
		<?php get_template_part( 'sidebar', 'content' ); ?>
		
	</div>
			
<?php 
get_footer();
?>