<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="shortcut icon" href="<?php echo get_site_url(); ?>/assets/logo_small-20141016-favicon.ico">
	
	<link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url"); ?>/css/normalize.css">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url"); ?>/css/header.css" media="screen, projection">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url"); ?>/css/main.css" media="screen, projection">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url"); ?>/css/footer.css" media="screen, projection">
	<!--  
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<!-- 
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
 	-->
	<?php wp_head(); ?>
	<?php comments_popup_script(); ?> 
	
	<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
	<!--[if lt IE 9]>
	
	<style>
	div.header > div > div.logo-name  {
	top: 40px;
	}
	div.header > div > div.search-box-wrapper > .search-box {top: 40px;
	}
	div.header > div.menu > div {top: 10px;
	}
	</style>
	<![endif]-->
	
</head>
<body <?php body_class(); ?> >
<div class="header">
	<div>
		<div class="logo">
			<a id="logo" href="<?php echo get_option('home'); ?>"></a>
		</div>
		<div class="logo-name"><a href="<?php echo get_option('home'); ?>"><?php bloginfo( 'name' ); ?></a></div>
		<div id="search-container" class="search-box-wrapper">
			<div class="search-box">
				<?php get_search_form(); ?>
			</div>
		</div>
	</div>
	
	<div class="menu" >
		<div><?php echo get_bloginfo ( 'description' )?></div>
	</div>
</div>
	
	
<div class="page-wrapper">
	

	