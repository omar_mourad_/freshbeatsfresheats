<?php
// add theme supports
add_theme_support( 'menus' );
//add_theme_support( 'custom-header' );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'woocommerce' );
set_post_thumbnail_size( 540, 649);

define("ENABLE_DEBUG_MODE", false);

function debug($filename) {
	if (ENABLE_DEBUG_MODE) {
		echo '<div id="pageName" style="z-index:1000;background:#000;position:absolute;top:1px;left:1px;display:block;color:red;">' . basename($filename) . '</div>';
	}
}

/**
 * Register cgp widget areas.
 *
 * @since cgp 1.0
 *
 * @return void
 */
function cgp_widgets_init() {
	//require get_template_directory() . '/inc/widgets.php';
	//register_widget( 'Twenty_Fourteen_Ephemera_Widget' );
	register_sidebar( array(
	'name'          => __( 'Home Page Image Slider', 'cgp' ),
	'id'            => 'home-page-slider-text',
	'description'   => __( 'Home Page Center Image Slider.', 'cgp' ),
	'before_widget' => '',
	'after_widget'  => '',
	'before_title'  => '',
	'after_title'   => '',
	) );
	register_sidebar( array(
		'name'          => __( 'Content Sidebar', 'cgp' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Additional sidebar that appears on the right.', 'cgp' ),
		'before_widget' => '<div id="%1$s" class="aside widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Widget Area', 'cgp' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'Appears in the footer section of the site.', 'cgp' ),
		'before_widget' => '<div id="%1$s" class="aside widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'cgp_widgets_init' );

add_filter( 'request', 'my_request_filter' );
function my_request_filter( $query_vars ) {
	if( isset( $_GET['s'] ) && empty( $_GET['s'] ) ) {
		$query_vars['s'] = " ";
	}
	return $query_vars;
}

// other code
add_shortcode( 'sidebar', 'sidebar_shortcode' );
function sidebar_shortcode( $atts ) {
	$page = '';
	// Attributes
	extract(
	shortcode_atts(
	array(
	'name' => '',
	), $atts
	)
	);

	if (is_active_sidebar($name)) {
		ob_start();
		dynamic_sidebar($name);
		$page = ob_get_contents();
		ob_end_clean();
	}

	return $page;
}

function get_limited_excerpt($excerpt, $maxLenght) {
	//$excerpt = get_the_content();
	$excerpt = strip_shortcodes($excerpt);
	$excerpt = strip_tags($excerpt);
	
	if (strlen($excerpt) > $maxLenght) {
		$excerpt = substr($excerpt, 0, $maxLenght) . '...';
	}
	if (empty($excerpt)) {
		$excerpt = '&nbsp;';
	}
	
	return $excerpt;
}

add_shortcode( 'add_top_button', 'add_top_button' );
function add_top_button( $atts ) {
	$html = '<input type="button" value="Top &uarr;" onclick="scroll(0,0);"/>';
	
	return $html;
}

add_shortcode( 'add_sidebar_social_item_button', 'add_sidebar_social_item_button' );
function add_sidebar_social_item_button($atts) {
	// Original Attributes, for filters
	$original_atts = $atts;
	
	// Pull in shortcode attributes and set defaults
	$atts = shortcode_atts( array(
			'type'              => ''
	), $atts );
	
	$type = sanitize_text_field( $atts['type'] );
	$class= '';
	switch ($type) {
		case 'twitter':
			$class = 'st_twitter_custom';
			break;
		case 'facebook':
			$class = 'st_facebook_custom';
			break;
		case 'pinterest':
			$class = 'st_pinterest_custom';
			break;
		case 'sharethis':
			$class = 'st_sharethis_custom';
			break;
	}
	
	$title = the_title('', '', false);
	$url = esc_url( apply_filters( 'the_permalink', get_permalink() ) );
	$html = <<<HTML
	<span class='$class' st_title='$title' st_url='$url'></span>
HTML;
	
	return $html;
}

// Create the shortcode
add_shortcode( 'cgp-display-posts', 'cgp_display_posts_shortcode' );
function cgp_display_posts_shortcode( $atts ) {

	// Original Attributes, for filters
	$original_atts = $atts;

	// Pull in shortcode attributes and set defaults
	$atts = shortcode_atts( array(
			'author'              => '',
			'category'            => '',
			'date_format'         => '(n/j/Y)',
			'special_date_format' => '',
			'id'                  => false,
			'ignore_sticky_posts' => false,
			'image_size'          => false,
			'include_title'       => true,
			'include_content'     => false,
			'include_date'        => false,
			'include_excerpt'     => false,
			'optional_message'    => '',
			'meta_key'            => '',
			'no_posts_message'    => '',
			'offset'              => 0,
			'order'               => 'DESC',
			'orderby'             => 'date',
			'post_parent'         => false,
			'post_status'         => 'publish',
			'post_type'           => 'post',
			'posts_per_page'      => '100',
			'tag'                 => '',
			'tax_operator'        => 'IN',
			'tax_term'            => false,
			'taxonomy'            => false,
			'wrapper'             => 'ul',
			'excerpt_size'        => 60,
			'title_size'          => 0,
			'include_wrapper'     => true,
	), $atts );

	$author = sanitize_text_field( $atts['author'] );
	$category = sanitize_text_field( $atts['category'] );
	$date_format = sanitize_text_field( $atts['date_format'] );
	$id = $atts['id']; // Sanitized later as an array of integers
	$ignore_sticky_posts = (bool) $atts['ignore_sticky_posts'];
	$image_size = sanitize_key( $atts['image_size'] );
	$include_title = (bool)$atts['include_title'];
	$include_content = (bool)$atts['include_content'];
	$include_date = (bool)$atts['include_date'];
	$include_excerpt = (bool)$atts['include_excerpt'];
	$meta_key = sanitize_text_field( $atts['meta_key'] );
	$no_posts_message = sanitize_text_field( $atts['no_posts_message'] );
	$offset = intval( $atts['offset'] );
	$order = sanitize_key( $atts['order'] );
	$orderby = sanitize_key( $atts['orderby'] );
	$post_parent = $atts['post_parent']; // Validated later, after check for 'current'
	$post_status = $atts['post_status']; // Validated later as one of a few values
	$post_type = sanitize_text_field( $atts['post_type'] );
	$posts_per_page = intval( $atts['posts_per_page'] );
	$tag = sanitize_text_field( $atts['tag'] );
	$tax_operator = $atts['tax_operator']; // Validated later as one of a few values
	$tax_term = sanitize_text_field( $atts['tax_term'] );
	$taxonomy = sanitize_key( $atts['taxonomy'] );
	$wrapper = sanitize_text_field( $atts['wrapper'] );
	$excerpt_size = intval( $atts['excerpt_size'] );
	$title_size = intval( $atts['title_size'] );
	$optional_message = ( $atts['optional_message'] );
	$special_date_format = ( $atts['special_date_format'] );
	
	// Set up initial query for post
	$args = array(
			'category_name'       => $category,
			'order'               => $order,
			'orderby'             => $orderby,
			'post_type'           => explode( ',', $post_type ),
			'posts_per_page'      => $posts_per_page,
			'tag'                 => $tag,
	);
	
	if ( strpos($image_size, 'custom_') !== false ) {
		$image_size = str_replace('custom_', '', $image_size);
		$image_size = split('_', $image_size);
	}
	
	// Ignore Sticky Posts
	if( $ignore_sticky_posts )
		$args['ignore_sticky_posts'] = true;

	// Meta key (for ordering)
	if( !empty( $meta_key ) )
		$args['meta_key'] = $meta_key;

	// If Post IDs
	if( $id ) {
		$posts_in = array_map( 'intval', explode( ',', $id ) );
		$args['post__in'] = $posts_in;
	}

	// Post Author
	if( !empty( $author ) )
		$args['author_name'] = $author;

	// Offset
	if( !empty( $offset ) )
		$args['offset'] = $offset;

	// Post Status
	$post_status = explode( ', ', $post_status );
	$validated = array();
	$available = array( 'publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash', 'any' );
	foreach ( $post_status as $unvalidated )
	if ( in_array( $unvalidated, $available ) )
		$validated[] = $unvalidated;
	if( !empty( $validated ) )
		$args['post_status'] = $validated;


	// If taxonomy attributes, create a taxonomy query
	if ( !empty( $taxonomy ) && !empty( $tax_term ) ) {

		// Term string to array
		$tax_term = explode( ', ', $tax_term );

		// Validate operator
		if( !in_array( $tax_operator, array( 'IN', 'NOT IN', 'AND' ) ) )
			$tax_operator = 'IN';
			
		$tax_args = array(
				'tax_query' => array(
						array(
								'taxonomy' => $taxonomy,
								'field'    => 'slug',
								'terms'    => $tax_term,
								'operator' => $tax_operator
						)
				)
		);

		// Check for multiple taxonomy queries
		$count = 2;
		$more_tax_queries = false;
		while(
				isset( $original_atts['taxonomy_' . $count] ) && !empty( $original_atts['taxonomy_' . $count] ) &&
				isset( $original_atts['tax_' . $count . '_term'] ) && !empty( $original_atts['tax_' . $count . '_term'] )
		):

		// Sanitize values
		$more_tax_queries = true;
		$taxonomy = sanitize_key( $original_atts['taxonomy_' . $count] );
		$terms = explode( ', ', sanitize_text_field( $original_atts['tax_' . $count . '_term'] ) );
		$tax_operator = isset( $original_atts['tax_' . $count . '_operator'] ) ? $original_atts['tax_' . $count . '_operator'] : 'IN';
		$tax_operator = in_array( $tax_operator, array( 'IN', 'NOT IN', 'AND' ) ) ? $tax_operator : 'IN';

		$tax_args['tax_query'][] = array(
				'taxonomy' => $taxonomy,
				'field' => 'slug',
				'terms' => $terms,
				'operator' => $tax_operator
		);

		$count++;
			
		endwhile;

		if( $more_tax_queries ):
		$tax_relation = 'AND';
		if( isset( $original_atts['tax_relation'] ) && in_array( $original_atts['tax_relation'], array( 'AND', 'OR' ) ) )
			$tax_relation = $original_atts['tax_relation'];
		$args['tax_query']['relation'] = $tax_relation;
		endif;

		$args = array_merge( $args, $tax_args );
	}

	// If post parent attribute, set up parent
	if( $post_parent ) {
		if( 'current' == $post_parent ) {
			global $post;
			$post_parent = $post->ID;
		}
		$args['post_parent'] = intval( $post_parent );
	}

	// Set up html elements used to wrap the posts.
	// Default is ul/li, but can also be ol/li and div/div
	$wrapper_options = array( 'ul', 'ol', 'div' );
	if( ! in_array( $wrapper, $wrapper_options ) )
		$wrapper = 'ul';
	$inner_wrapper = 'div' == $wrapper ? 'div' : 'li';


	$listing = new WP_Query( apply_filters( 'display_posts_shortcode_args', $args, $original_atts ) );
	if ( ! $listing->have_posts() )
		return apply_filters( 'display_posts_shortcode_no_results', wpautop( $no_posts_message ) );

	return cgp_display_posts_shortcode_display($listing, $atts);
}

function cgp_display_posts_shortcode_display(WP_Query $listing, $atts) {
	$atts = shortcode_atts( array(
			'date_format'         => '(n/j/Y)',
			'special_date_format' => '',
			'image_size'          => false,
			'include_title'       => true,
			'include_content'     => false,
			'include_date'        => false,
			'include_excerpt'     => false,
			'include_image'       => false,
			'optional_message'    => '',
			'no_posts_message'    => '',
			'wrapper'             => 'ul',
			'excerpt_size'        => 60,
			'title_size'          => 0,
			'include_wrapper'     => true,
			'include_recipe'     => true,
	), $atts );
	
	$date_format = sanitize_text_field( $atts['date_format'] );
	$image_size = sanitize_key( $atts['image_size'] );
	$include_title = (bool)$atts['include_title'];
	$include_content = (bool)$atts['include_content'];
	$include_date = (bool)$atts['include_date'];
	$include_excerpt = (bool)$atts['include_excerpt'];
	$no_posts_message = sanitize_text_field( $atts['no_posts_message'] );
	$offset = intval( $atts['offset'] );
	$wrapper = sanitize_text_field( $atts['wrapper'] );
	$excerpt_size = intval( $atts['excerpt_size'] );
	$title_size = intval( $atts['title_size'] );
	$optional_message = ( $atts['optional_message'] );
	$special_date_format = ( $atts['special_date_format'] );
	$include_wrapper = (bool)$atts['include_wrapper'];
	$include_image = (bool)$atts['include_image'];
	$include_recipe = (bool)$atts['include_recipe'];
	
	if ( strpos($image_size, 'custom_') !== false ) {
		$image_size = str_replace('custom_', '', $image_size);
		$image_size = split('_', $image_size);
	}
	
	// Set up html elements used to wrap the posts.
	// Default is ul/li, but can also be ol/li and div/div
	$wrapper_options = array( 'ul', 'ol', 'div' );
	if( ! in_array( $wrapper, $wrapper_options ) )
		$wrapper = 'ul';
	$inner_wrapper = 'div' == $wrapper ? 'div' : 'li';
	
	$inner = '';
	while ( $listing->have_posts() ): $listing->the_post(); global $post;

	$class = array( 'listing-item' );
	$class = apply_filters( 'display_posts_shortcode_post_class', $class, $post, $listing );
	if (!empty($optional_message)) {
		$parsedOutput = $optional_message;
		
		if ($include_recipe) {
			//%recipe_cook_time%
			$recipe = get_post_custom($post->ID);
			$parsedOutput = str_replace('%recipe_cook_time%', $recipe['recipe_cook_time'][0], $parsedOutput);
			$parsedOutput = str_replace('%recipe_prep_time_text%', $recipe['recipe_prep_time_text'][0], $parsedOutput);
			$parsedOutput = str_replace('%recipe_servings%', $recipe['recipe_servings'][0], $parsedOutput);
			$parsedOutput = str_replace('%recipe_servings_type%', $recipe['recipe_servings_type'][0], $parsedOutput);
		}
		
		if ( $include_date ) {
			$parsedOutput = str_replace('%date%', get_the_date( $date_format ), $parsedOutput);
		}
		if ($include_title) {
			$parsedOutput = str_replace('%title%', apply_filters( 'the_title', processTitle(get_the_title(), $title_size) ), $parsedOutput);
		}
		if ($include_excerpt) {
			$parsedOutput = str_replace('%excerpt%', get_limited_excerpt(get_the_content(), $excerpt_size), $parsedOutput);
		}
		//if ($include_image) {
			if ($image_size && has_post_thumbnail()) {
				$parsedOutput = str_replace('%thumbnail%', get_the_post_thumbnail( $post->ID, $image_size ), $parsedOutput);
			} else {
				$parsedOutput = str_replace('%thumbnail%', '', $parsedOutput);
			}/*
		} else {
			$parsedOutput = str_replace('%thumbnail%', '', $parsedOutput);
		}
		*/
		if( $include_content ) {
			$parsedOutput = str_replace('%content%', apply_filters( 'the_content', get_the_content() ), $parsedOutput);
		}
		
		$parsedOutput = str_replace('%author%', get_the_author(), $parsedOutput);
		$parsedOutput = str_replace('%totalComments%', get_comments_number(), $parsedOutput);
		$parsedOutput = str_replace('%url%', get_permalink(), $parsedOutput);
		
		if ( !empty($special_date_format) ) {
			$special_date_format = '\<\s\p\a\n\>D\<\/\s\p\a\n\>\<\s\p\a\n\>d\<\/\s\p\a\n\>';
			$parsedOutput = str_replace('%specialFormatedDate%', get_the_date( $special_date_format ), $parsedOutput);
		}
		
		if ($include_wrapper) {
			$output = '<' . $inner_wrapper . ' class="' . implode( ' ', $class ) . '">' . $parsedOutput . '</' . $inner_wrapper . '>';
		} else {
			$output = $parsedOutput;
		}
		$output = do_shortcode($output);
	} else {
		
		$image = $date = $excerpt = $content = '';
		
		//$title = '<a class="title" href="' . apply_filters( 'the_permalink', get_permalink() ) . '">' . apply_filters( 'the_title', get_the_title() ) . '</a>';
		if ($include_title) {
			$title = ' <p class="title">' . apply_filters( 'the_title', processTitle(get_the_title(), $title_size) ) . '</p>';
		}
	
		if ( $image_size && has_post_thumbnail() )
			$image = '<a class="image" href="' . get_permalink() . '">' . get_the_post_thumbnail( $post->ID, $image_size ) . '</a> ';
			
		if ( $include_date )
			$date = ' <span class="date"><span>' . get_the_date( $date_format ) . '</span><div><div class="arrow-down" ></div></div></span>';
	
		if ( $include_excerpt )
			$excerpt = ' <span class="excerpt">' . get_limited_excerpt(get_the_content(), $excerpt_size) . '</span>';
			
		if( $include_content ) {
			$content = '<div class="content">' . apply_filters( 'the_content', get_the_content() ) . '</div>';
			$content = do_shortcode($content);
		}

		$output = '<' . $inner_wrapper . ' class="' . implode( ' ', $class ) . '">' . $image . $date . $title . $excerpt . $content . '</' . $inner_wrapper . '>';
		
		
	}
	if ($include_wrapper) {
		$output = '<a class="title" href="' . apply_filters( 'the_permalink', get_permalink() ) . '">' . $output . '</a>';
		
		// If post is set to private, only show to logged in users
		if( 'private' == get_post_status( $post->ID ) && !current_user_can( 'read_private_posts' ) )
			$output = '';
		
		$inner .= apply_filters( 'display_posts_shortcode_output', $output, $original_atts, $image, $title, $date, $excerpt, $inner_wrapper, $content, $class );
		
	} else {
		$inner .= $output;
	}
	
	endwhile; wp_reset_postdata();

	if ($include_wrapper) {
		$open = apply_filters( 'display_posts_shortcode_wrapper_open', '<' . $wrapper . ' class="display-posts-listing">', $original_atts );
		$close = apply_filters( 'display_posts_shortcode_wrapper_close', '</' . $wrapper . '>', $original_atts );
		$return = $open . $inner . $close;
	} else {
		$return = $inner;
	}
	
	return $return;
}

function processTitle($title, $title_size) {
	if ($title_size > 0) {
		$title = get_limited_excerpt($title, $title_size);
	}
	
	return $title;
}

function cgp_content_pagination() {
	
	global $page, $numpages, $multipage, $more, $pagenow;

	$output = '';
	if ( $multipage ) {
		for ($i = 1; $i < ($numpages); $i++) {
			if ($page == $i) {
				$output .= '<span class="current">' . $i . '</span>';
			} else {
				$output .= _wp_link_page($i) . $i . '</a>';
			}
		}
		if ($numpages > 0 && $page != $numpages) {
			$output .= _wp_link_page($numpages) . 'End</a>';
		} else {
			$output .= '<span class="current">End</span>';
		}
	}

	return $output;
}

function cgp_get_the_term_list( $id, $taxonomy, $before = '', $sep = '', $after = '', $urlParams = '') {
	$terms = get_the_terms( $id, $taxonomy );

	if ( is_wp_error( $terms ) )
		return $terms;

	if ( empty( $terms ) )
		return false;

	foreach ( $terms as $term ) {
		$link = get_term_link( $term, $taxonomy ) . $urlParams;
		if ( is_wp_error( $link ) )
			return $link;
		$term_links[] = '<a href="' . esc_url( $link ) . '" rel="tag">' . $term->name . '</a>';
	}

	/**
	 * Filter the term links for a given taxonomy.
	 *
	 * The dynamic portion of the filter name, $taxonomy, refers
	 * to the taxonomy slug.
	 *
	 * @since 2.5.0
	 *
	 * @param array $term_links An array of term links.
	 */
	$term_links = apply_filters( "term_links-$taxonomy", $term_links );

	return $before . join( $sep, $term_links ) . $after;
}

/********************************************************/
function pagination($pages = '', $range = 4)
{
	$showitems = ($range * 2)+1;

	// current page
	global $paged;
	if(empty($paged)) 
		$paged = 1;

	// max pages
	if($pages == '')
	{
		global $wp_query;
		$pages = $wp_query->max_num_pages;
		if(!$pages)
		{
			$pages = 1;
		}
	}

	if(1 != $pages)
	{
		if($paged > 2 && $paged > $range+1 && $showitems < $pages)
			echo "<a href='".get_pagenum_link(1)."'>1</a>";
	
		if ($paged - $range > 2) {
			echo "...";
		}

		for ($i=1; $i <= $pages; $i++)
		{
			if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
			{
				echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
			}
		}
		
        if ($paged + $range < $pages) {
       		echo "...";
       		$i -= 2;
        	echo "<a href='".get_pagenum_link($i)."'>".$i."</a>";
        }

        if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) 
        	echo "<a href='".get_pagenum_link($pages)."'>End</a>";
        
        echo "</div>\n";
	}
	
	
}

/**********************************************************************/

class Base_Walker_Nav_Menu extends Walker_Nav_Menu {
	
	protected function getMenuItemData($item, $parentSection) {
		$url = esc_attr( $item->url);
		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
		
		return $attributes;
	}
}

class cgp_Walker_Main_Menu extends Base_Walker_Nav_Menu {
	private $parentSection;
	
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
		
		if ($item->attr_title === 'invisible') {
			return;
		}
	
		$class_names = $value = '';
	
		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;

		$titleClassName .=  ' ' . str_replace(' ', '_', strtolower(trim($item->title)));
		
		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . ' ' . $titleClassName . '"' : '';
	
		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
		$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';
		
		$output .= $indent . '<li' . $id . $value . $class_names .'>';
		
		if ($item->menu_item_parent==0) {
			$this->parentSection = $item;
		}
		
		$attributes = $this->getMenuItemData($item, $this->parentSection);
	
		$item_output = '';
		//if (strpos($class_names, 'current-page-ancestor') != false || strpos($class_names, 'current-menu-item') != false ) {
		if (strpos($class_names, 'menu-item-has-children') != false) {
			$item_output .= $args->selectedItem;
		}
		
		$item_output .= $args->before;
		$item_output .= '<a'. $attributes .'>';
		
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= '</a>';
		$item_output .= $args->after;
	
		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}

class Custom_Walker_Nav_Sub_Menu extends Base_Walker_Nav_Menu {

	private $found_parents = array();
	public $parentSection = null;
	private $hasSpecialMenu = false;
	private $disableMenu = false;
	private $menuadded = false;
	private $startElm = true;
	public $breadCrumbPath = array();
	
	function start_el(&$output, $item, $depth, $args) {

		global $wp_query;

		//this only works for second level sub navigations
		$parent_item_id = 0;
		
		$class_names = $value = '';
		
		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		
		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
		$class_names = ' class="' . esc_attr( $class_names ) . '"';
		
		if (strpos($class_names, 'current-menu-ancestor') && $item->menu_item_parent==0) {
			$this->parentSection = $item;
			$this->hasSpecialMenu = ($item->attr_title == 'specialMenu')?true:false;
			$this->disableMenu = ($item->attr_title == 'disableMenu')?true:false;
		} elseif (strpos($class_names, 'current_page_item') && $item->menu_item_parent==0) {
			$this->parentSection = $item;
		}
		
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		if ($this->disableMenu) {
			return'';
		}
		// Checks if the current element is in the current selection
		if (strpos($class_names, 'current-menu-item')
		|| strpos($class_names, 'current-menu-parent')
		|| strpos($class_names, 'current-menu-ancestor')
		|| (is_array($this->found_parents) && in_array( $item->menu_item_parent, $this->found_parents )) ) {

			// Keep track of all selected parents
			if (strpos($class_names, 'current-menu-ancestor') || strpos($class_names, 'current_page_item')) {
				$this->found_parents[] = $item->ID;
				$this->breadCrumbPath[] = array('title' => $item->title, 'url' => esc_attr($item->url));
			}

			//check if the item_parent matches the current item_parent
			if($item->menu_item_parent!=$parent_item_id){
				
				if ($this->hasSpecialMenu && $depth == 1 && !(strpos($class_names, 'current_page_ancestor') || strpos($class_names, 'current_page_item'))) {
					return;
				}
				
				if (!empty($output)) {
					$output .= '</label></div>';
				}
				
				$class = '';
				$class .= ' lvl'.$depth;
				
				if (strpos($class_names, 'current_page_item')) {
					$class .= ' active_page';
				}
				if (strpos($class_names, 'current_page_item') || (strpos($class_names, 'current_page_ancestor'))) {
					$class .= ' active';
				}
				
				$class = trim($class);
				
				$minDepth = 2;
				if ($this->hasSpecialMenu) {
					$minDepth = 3;
				}
				
				if ($this->hasSpecialMenu && strpos($class_names, 'current_page_ancestor') && $depth == 1) {
					$class = ' lvl'.$depth;
				}
								
				if (!empty($output) && $depth < $minDepth) {
					$output .= '</div><div class="container ' . $class . '">';
				}
				
				if ($this->startElm) {
					$this->startElm = false;
					$output .= '<div class="container ' . $class . '">';
				}
				
				$attributes = $this->getMenuItemData($item, $this->parentSection);
				
				$attributes = !empty( $item->url )? "javascript:document.location.href='" . esc_attr($item->url)  . "';":'';
				$output .= $indent . '<div onclick="' . $attributes . '" class="' . $class . '" ><input type="radio" /><label><span></span>';
				
				$item_output =  '<div>' . apply_filters( 'the_title', $item->title, $item->ID ) . '</div>';
				
				$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
			}


		}
	}
	
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		//$indent = str_repeat("\t", $depth);
		//$output .= "\n$indent<ul class=\"sub-menu\">\n";
		//$output .= '</label></div>';
	}

	function end_el(&$output, $item, $depth) {
		
		// Closes only the opened li
		if ( is_array($this->found_parents) && in_array( $item->ID, $this->found_parents ) ) {
			//$output .= "</li>\n";
		}
		
	}

	function end_lvl(&$output, $depth) {
		$indent = str_repeat("\t", $depth);
		// If the sub-menu is empty, strip the opening tag, else closes it
		if (substr($output, -22)=="<ul class=\"sub-menu\">\n") {
			//$output = substr($output, 0, strlen($output)-23);
		} else {
			//$output .= "$indent</ul>\n";
		}
	}

}

class MenuBreadCrumbService {
	public $navHtml = '';
	public $navSectionName = '';
	public $breadcrumbHtml = '';
	private static $instance = null;
	
	private function __construct() {
			
	}
	
	public static function getInstance() {
		
		if (MenuBreadCrumbService::$instance === null) {
			MenuBreadCrumbService::$instance = new MenuBreadCrumbService();
		}
		return MenuBreadCrumbService::$instance;
	}
	
	public function init() {
		// add the title attribute "specialMenu" to the special physycian menu
		$navMenu = new Custom_Walker_Nav_Sub_Menu();
		$menu_args = array(
				'walker' => $navMenu,
				'container' => '',
				'items_wrap' => '%3$s',
				'echo' => false
		);
		$this->navHtml = wp_nav_menu($menu_args);
		
		$navSection = $navMenu->parentSection;
		$this->navSectionName = '';
		if ($navSection !== null) {
			$this->navSectionName = empty($navSection->post_title) ? $navSection->title: $navSection->post_title;
		}
		if (empty($this->navHtml)) {
			$this->navHtml = '<div><div><label>';
		}
		$this->breadcrumbHtml = $this->menu_breadcrumb($navMenu->breadCrumbPath);
	}
	
	private function menu_breadcrumb($a) {
	
		$s = '<div id="breadcrumb"><a href="'.get_bloginfo('url').'">Home</a> > ';
		
		$lastItem = array_pop($a);
		
		foreach ($a as $v) {
			$url = $v['url'];/*
			if ($url == '#') {
				continue;
			}*/
			if ($url == '#') {
				$s .= $v['title'] . ' > ';
			} else {
				$s .= '<a href=' . $url . ' title=' . $v['title'] . '>' . $v['title'] . '</a> > ';
			}
			
		}
		
		$s .= '<span class="currentPage">' . $lastItem['title'] . '</span>';
		$s .= '</div>';
	
		return $s;
	
	}
}

class BlogBreadCrumbService {
	public $breadcrumbHtml = '';

	public function init($pathList = array()) {
		$breadCrumbPath = array();
		
		if (!empty($pathList)) {
			$breadCrumbPath = $pathList;
		} else {
			$breadCrumbPath[] = array('title' => 'Blog', 'url' => '/blog');
			$breadCrumbPath[] = array('title' => get_the_title(), 'url' => '');
		}
		$this->breadcrumbHtml = $this->menu_breadcrumb($breadCrumbPath);
		
	}

	function menu_breadcrumb($a) {
		$s = '<div id="breadcrumb"><a href="'.get_bloginfo('url').'">Home</a> > ';

		$lastItem = array_pop($a);
		foreach ($a as $v) {
			$url = $v['url'];
			if ($url == '#') {
				continue;
			}
			$s .= '<a href=' . $url . ' title=' . $v['title'] . '>' . $v['title'] . '</a> > ';
		}
		
		$s .= '<span class="currentPage">' . $lastItem['title'] . '</span>';
		$s .= '</div>';

		return $s;

	}
}

class TitleImageCreator {
	private static $images = array();
	
	private static function getImageList() {
		if (count(TitleImageCreator::$images) == 0) {
			// create list
			TitleImageCreator::$images['about-us'] = '/assets/masthead2.png';
			
			TitleImageCreator::$images['retail'] = '/assets/masthead3.png';
			TitleImageCreator::$images['product-category'] = '/assets/masthead3.png';
			TitleImageCreator::$images['product'] = '/assets/masthead3.png';
			TitleImageCreator::$images['point-of-sale'] = '/assets/masthead3.png';
			
			TitleImageCreator::$images['food-service'] = '/assets/masthead4.png';
			
			TitleImageCreator::$images['product-lists-download'] = '/assets/masthead4.png';
			
			TitleImageCreator::$images['news'] = '/assets/masthead1.png';
			TitleImageCreator::$images['all-news'] = '/assets/masthead1.png';
		}
		return TitleImageCreator::$images;
	}
	public static function getImage($url) {
		$images = TitleImageCreator::getImageList();
		$titleImage = '/assets/masthead5.png';
		
		$url = str_replace('/~cgp/', '', $url);
		$parts = split('/', $url);
		
		if (isset($images[$parts[1]])) {
			return $images[$parts[1]];
		} elseif (isset($images[$parts[0]])) {
			return $images[$parts[0]];
		}
		
		return $titleImage;
	}
	
}

/*
add_action('init', 'cptui_register_my_cpt_footer_content');
function cptui_register_my_cpt_footer_content() {
	register_post_type('footer_content', array(
			'label' => 'Footer sections',
			'description' => '',
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'capability_type' => 'post',
			'map_meta_cap' => true,
			'hierarchical' => false,
			'rewrite' => array('slug' => 'footer_content', 'with_front' => true),
			'query_var' => true,
			'supports' => array('title','editor','excerpt','trackbacks','custom-fields','comments','revisions','thumbnail','author','page-attributes','post-formats'),
			'labels' => array (
				'name' => 'Footer sections',
				'singular_name' => '',
				'menu_name' => 'Footer sections',
				'add_new' => 'Add Footer sections',
				'add_new_item' => 'Add New Footer sections',
				'edit' => 'Edit',
				'edit_item' => 'Edit Footer sections',
				'new_item' => 'New Footer sections',
				'view' => 'View Footer sections',
				'view_item' => 'View Footer sections',
				'search_items' => 'Search Footer sections',
				'not_found' => 'No Footer sections Found',
				'not_found_in_trash' => 'No Footer sections Found in Trash',
				'parent' => 'Parent Footer sections',
			)
		)
	); 
}

*/

